window.navigateToSection = function (page, sectionId) {
  window.location.href = `${page}#${sectionId}`;
};

fetch("navBar.html")
  .then((response) => response.text())
  .then((text) => {
    document.getElementById("navbar-placeHolder").innerHTML = text;
  });

fetch("cookies.html")
  .then((response) => response.text())
  .then((text) => {
    document.getElementById("cookies").innerHTML = text;
  });

document.addEventListener("DOMContentLoaded", function () {
  const images = [
    "./imgZahPot/zahpot1.jpg",
    "./imgZahPot/zahpot2.jpg",
    "./imgZahPot/zahpot3.jpg",
    "./imgZahPot/zahpot4.jpg",
    "./imgZahPot/zahpot5.jpg",
    "./imgZahPot/zahpot6.jpg",
    "./imgZahPot/zahpot7.jpg",
    "./imgZahPot/zahpot8.jpg",
    "./imgZahPot/zahpot9.jpg",
    "./imgZahPot/zahpot10.jpg",
    "./imgZahPot/zahpot11.jpg",
    "./imgZahPot/zahpot12.jpg",
    "./imgZahPot/zahpot13.jpg",
    "./imgZahPot/zahpot14.jpg",
    "./imgZahPot/zahpot15.jpg",
  ];

  const gallery = document.getElementById("gallery");
  const modal = document.getElementById("modal");
  const modalImg = document.getElementById("modal-img");

  // Add images dynamically
  images.forEach((src, index) => {
    const img = document.createElement("img");
    img.src = src;
    img.alt = `Image ${index + 1}`;
    img.classList.add(
      "w-full",
      "h-32",
      "object-cover",
      "rounded-lg",
      "cursor-pointer",
      "hover:scale-105",
      "transition-transform"
    );
    img.addEventListener("click", () => {
      modal.classList.remove("hidden");
      modalImg.src = src;
    });
    gallery.appendChild(img);
  });

  // Close modal on click
  modal.addEventListener("click", () => {
    modal.classList.add("hidden");
  });
});