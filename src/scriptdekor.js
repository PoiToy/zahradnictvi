window.navigateToSection = function (page, sectionId) {
  window.location.href = `${page}#${sectionId}`;
};

fetch("navBar.html")
  .then((response) => response.text())
  .then((text) => {
    document.getElementById("navbar-placeHolder").innerHTML = text;
  });

fetch("cookies.html")
  .then((response) => response.text())
  .then((text) => {
    document.getElementById("cookies").innerHTML = text;
  });

document.addEventListener("DOMContentLoaded", function () {
  const images = [
    "./imgDek/dek1.jpg",
    "./imgDek/dek2.jpg",
    "./imgDek/dek3.jpg",
    "./imgDek/dek4.jpg",
    "./imgDek/dek5.jpg",
    "./imgDek/dek6.jpg",
    "./imgDek/dek7.jpg",
    "./imgDek/dek8.jpg",
    "./imgDek/dek9.jpg",
    "./imgDek/dek10.jpg",
    "./imgDek/dek11.jpg",
    "./imgDek/dek12.jpg",
    "./imgDek/dek13.jpg",
    "./imgDek/dek14.jpg",
    "./imgDek/dek15.jpg",
    "./imgDek/dek16.jpg",
  ];

  const gallery = document.getElementById("gallery");
  const modal = document.getElementById("modal");
  const modalImg = document.getElementById("modal-img");

  // Add images dynamically
  images.forEach((src, index) => {
    const img = document.createElement("img");
    img.src = src;
    img.alt = `Image ${index + 1}`;
    img.classList.add(
      "w-full",
      "h-32",
      "object-cover",
      "rounded-lg",
      "cursor-pointer",
      "hover:scale-105",
      "transition-transform"
    );
    img.addEventListener("click", () => {
      modal.classList.remove("hidden");
      modalImg.src = src;
    });
    gallery.appendChild(img);
  });

  // Close modal on click
  modal.addEventListener("click", () => {
    modal.classList.add("hidden");
  });
});