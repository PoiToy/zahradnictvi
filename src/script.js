// General navigation functions
window.getGallery = function () {
  window.location.href = "gallery.html";
};

window.getOrderRequest = function () {
  window.location.href = "orderRequest.html";
};

window.getThankYouPageEshop = function () {
  window.location.href = "thankYouPageEshop.html";
};

window.getThankYouPage = function () {
  window.location.href = "thankYouPage.html";
};

window.navigateToSection = function (page, sectionId) {
  window.location.href = `${page}#${sectionId}`;
};

window.getPerennials = function () {
  window.location.href = "trvalky.html";
}

window.getDecorations = function () {
  window.location.href = "dekorace.html";
}

window.getTools = function () {
  window.location.href = "zahradnickepotreby.html";
}

window.getTrees = function () {
  window.location.href = "stromy.html";
}

window.getHerbs = function () {
  window.location.href = "bylinkyazelenina.html";
};

fetch("navBar.html")
  .then((response) => response.text())
  .then((text) => {
    document.getElementById("navbar-placeHolder").innerHTML = text;
  });

  fetch("cookies.html")
  .then((response) => response.text())
  .then((text) => {
    document.getElementById("cookies").innerHTML = text;
  });

document.addEventListener("DOMContentLoaded", function () {
  const images = [
    "./img/obchod0.jpg",
    "./img/obchod1.jpg",
    "./img/obchod2.jpg",
    "./img/obchod3.jpg",
    "./img/obchod4.jpg",
    "./img/obchod5.jpg",
    "./img/obchod6.jpg",
    "./img/obchod7.jpg",
    "./img/obchod8.jpg",
    "./img/obchod9.jpg",
    "./img/obchod10.jpg",
    "./img/obchod11.jpg",
    "./img/obchod12.jpg",
    "./img/obchod13.jpg",
    "./img/obchod14.jpg",
    "./img/obchod15.jpg",
    "./img/obchod16.jpg",
    "./img/obchod17.jpg",
    "./img/obchod18.jpg",
    "./img/obchod19.jpg",
    "./img/obchod20.jpg",
    "./img/obchod21.jpg",
    "./img/obchod22.jpg",
    "./img/obchod23.jpg",
    "./img/obchod24.jpg",
  ];

  const galleries = document.getElementById("galleries");
  const modal = document.getElementById("modal");
  const modalImg = document.getElementById("modal-img");

  // Add images dynamically
  images.forEach((src, index) => {
    const img = document.createElement("img");
    img.src = src;
    img.alt = `Image ${index + 1}`;
    img.classList.add(
      "w-full",
      "h-32",
      "object-cover",
      "rounded-lg",
      "cursor-pointer",
      "hover:scale-105",
      "transition-transform"
    );
    img.addEventListener("click", () => {
      modal.classList.remove("hidden");
      modalImg.src = src;
    });
    galleries.appendChild(img);
  });

  // Close modal on click
  modal.addEventListener("click", () => {
    modal.classList.add("hidden");
  });
});

// Function to show the image and hide the main content - After press text, will apear picture // NOT USED NOW
function showImage() {
  document.getElementById("imageContent").style.display = "flex";
}

// Hide the image and go back to the main screen
function showMain() {
  document.getElementById("imageContent").style.display = "none";
}

function copyToClipboard(text) {
  navigator.clipboard.writeText(text).then(() => {
      alert('Emailová adresa zkopírována do schránky');
  }).catch(err => {
      console.error('Nelze zkopírovat: ', err);
  });
}
